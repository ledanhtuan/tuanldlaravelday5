<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 50)->nullable();
            $table->string('email', 191)->unique()->nullable();
            $table->string('phone', 191)->unique()->nullable();
            $table->integer('role')->nullable();
            $table->tinyInteger('active')->nullable()->default('1');
            $table->bigInteger('creator_id')->default(null);
            $table->bigInteger('updater_id')->default(null);
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->string('tmp_password', 191);
            $table->integer('login_fails')->nullable()->default('0');
            $table->timestamp('last_login_fail_at')->default(null);
            $table->timestamp('last_login')->default(null);
            $table->string('profile_image', 500)->default(null);
            $table->unsignedBigInteger('member_id')->unique();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};