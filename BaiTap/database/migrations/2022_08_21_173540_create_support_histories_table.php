<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('support_histories', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('admin_id');
            $table->bigInteger('number_id');
            $table->datetime('datetime')->nullable();
            $table->string('content', 2000)->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('support_histories');
    }
};