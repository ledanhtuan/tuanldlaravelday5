<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_histories', function (Blueprint $table) {
            $table->bigInteger('id', true);
            $table->bigInteger('b_tour_order_id')->unsigned();
            $table->bigInteger('b_tour_order_schedule_id')->unsigned();
            $table->date('schedule_date')->nullable();
            $table->tinyInteger('is_draft_reserved')->nullable();
            $table->tinyInteger('is_final_reserved')->nullable();
            $table->text('reserved_content')->nullable();
            $table->bigInteger('admin_id');
            $table->string('admin_name', 191);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_histories');
    }
};